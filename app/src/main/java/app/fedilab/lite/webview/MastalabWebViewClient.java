package app.fedilab.lite.webview;
/* Copyright 2017 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */


import android.app.Activity;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import app.fedilab.lite.R;
import app.fedilab.lite.activities.WebviewActivity;
import app.fedilab.lite.helper.Helper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * Created by Thomas on 25/06/2017.
 * Custom WebViewClient
 */

public class MastalabWebViewClient extends WebViewClient {

    private Activity activity;
    private int count = 0;

    public MastalabWebViewClient(Activity activity) {
        this.activity = activity;
    }

    public List<String> domains = new ArrayList<>();

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
    }


    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request){

        String url = request.getUrl().toString();
        if (WebviewActivity.trackingDomains != null) {
            URI uri;
            try {
                uri = new URI(url);
                String domain = uri.getHost();
                if (domain != null) {
                    domain = domain.startsWith("www.") ? domain.substring(4) : domain;
                }
                if (domain != null && WebviewActivity.trackingDomains.contains(domain)) {
                    if (activity instanceof WebviewActivity) {
                        count++;
                        domains.add(url);
                        ((WebviewActivity) activity).setCount(activity, String.valueOf(count));
                    }
                    ByteArrayInputStream nothing = new ByteArrayInputStream("".getBytes());
                    return new WebResourceResponse("text/plain", "utf-8", nothing);
                }
            } catch (URISyntaxException e) {
                try {
                    url = url.substring(0, 50);
                    uri = new URI(url);
                    String domain = uri.getHost();
                    if (domain != null) {
                        domain = domain.startsWith("www.") ? domain.substring(4) : domain;
                    }
                    if (domain != null && WebviewActivity.trackingDomains.contains(domain)) {
                        if (activity instanceof WebviewActivity) {
                            count++;
                            domains.add(url);
                            ((WebviewActivity) activity).setCount(activity, String.valueOf(count));
                        }
                        ByteArrayInputStream nothing = new ByteArrayInputStream("".getBytes());
                        return new WebResourceResponse("text/plain", "utf-8", nothing);

                    }
                } catch (URISyntaxException ignored) {}
            }
        }
        request.getRequestHeaders().put("DNT","1");
        request.getRequestHeaders().put("User-Agent",random(50));
        return super.shouldInterceptRequest(view, request);
    }


    public List<String> getDomains() {
        return this.domains;
    }

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        String instance = Helper.getLiveInstance(activity);
        if (instance != null && instance.endsWith(".onion")) {
            handler.proceed();
        } else {
            super.onReceivedSslError(view, handler, error);
        }
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request){
        String url = request.getUrl().toString();
        if (URLUtil.isNetworkUrl(url)) {
            return false;
        } else {
            view.stopLoading();
            view.goBack();
        }
        return false;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        count = 0;
        domains = new ArrayList<>();
        domains.clear();
        ActionBar actionBar = ((AppCompatActivity) activity).getSupportActionBar();
        LayoutInflater mInflater = LayoutInflater.from(activity);
        if (actionBar != null) {
            View webview_actionbar = mInflater.inflate(R.layout.webview_actionbar, new LinearLayout(activity), false);
            TextView webview_title = webview_actionbar.findViewById(R.id.webview_title);
            webview_title.setText(url);
            actionBar.setCustomView(webview_actionbar);
            actionBar.setDisplayShowCustomEnabled(true);
        } else {
            activity.setTitle(url);
        }
        //Changes the url in webview activity so that it can be opened with an external app
        try {
            if (activity instanceof WebviewActivity)
                ((WebviewActivity) activity).setUrl(url);
        } catch (Exception ignore) {
        }

    }


    public static String random(int count) {
        char[] chars = "abcdefghijklmnopqrstuvwxyz-_/".toCharArray();
        StringBuilder sb = new StringBuilder(count);
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }
}
