<?xml version="1.0" encoding="utf-8"?><!--
    Copyright 2019 Thomas Schneider

    This file is a part of Fedilab

    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU General Public License as published by the Free Software Foundation; either version 3 of the
    License, or (at your option) any later version.

    Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
    Public License for more details.

    You should have received a copy of the GNU General Public License along with Fedilab; if not,
    see <http://www.gnu.org/licenses>.
-->

<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    xmlns:TagsEditText="http://schemas.android.com/apk/res-auto"
    android:layout_marginLeft="@dimen/fab_margin"
    android:layout_marginStart="@dimen/fab_margin"
    android:layout_marginRight="@dimen/fab_margin"
    android:layout_marginEnd="@dimen/fab_margin"
    tools:context="app.fedilab.lite.activities.PeertubeUploadActivity">

    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical">

            <!-- Video title -->
            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="vertical">

                <TextView
                    android:labelFor="@+id/p_video_title"
                    android:text="@string/title"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content" />

                <EditText
                    android:id="@+id/p_video_title"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:inputType="text" />
            </LinearLayout>

            <!-- Video tags -->
            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="vertical">

                <TextView
                    android:labelFor="@+id/p_video_tags"
                    android:text="@string/tags"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content" />

                <mabbas007.tagsedittext.TagsEditText
                    android:id="@+id/p_video_tags"
                    android:layout_width="match_parent"
                    android:minLines="2"
                    android:lines="2"
                    android:layout_height="wrap_content"
                    TagsEditText:allowSpaceInTag="true"
                    TagsEditText:tagsCloseImageRight="@drawable/tag_close"
                    TagsEditText:tagsTextColor="?colorAccent"
                    TagsEditText:tagsTextSize="@dimen/defaultTagsTextSize"
                    TagsEditText:tagsCloseImagePadding="@dimen/defaultTagsCloseImagePadding"
                    android:inputType="textMultiLine" />
            </LinearLayout>


            <!-- video description -->
            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="vertical">

                <TextView
                    android:labelFor="@+id/p_video_description"
                    android:text="@string/description"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content" />

                <EditText
                    android:inputType="textMultiLine"
                    android:id="@+id/p_video_description"
                    android:layout_width="match_parent"
                    android:lines="5"
                    android:maxLines="5"
                    android:layout_height="wrap_content"
                    android:scrollbars="vertical" />
            </LinearLayout>

            <!-- Videos channels -->
            <LinearLayout
                android:layout_marginTop="20dp"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center_vertical"
                android:layout_gravity="center_horizontal"
                android:orientation="vertical">

                <TextView
                    android:gravity="center"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:text="@string/channel" />

                <com.jaredrummler.materialspinner.MaterialSpinner
                    android:id="@+id/set_upload_channel"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content" />
            </LinearLayout>


            <!-- Videos categories -->
            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="10dp"
                android:gravity="center_vertical"
                android:layout_gravity="center_vertical"
                android:orientation="vertical">

                <TextView
                    android:gravity="center"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:text="@string/category" />

                <com.jaredrummler.materialspinner.MaterialSpinner
                    android:id="@+id/set_upload_categories"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content" />
            </LinearLayout>


            <!-- Videos licences -->
            <LinearLayout
                android:layout_marginTop="10dp"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center_vertical"
                android:layout_gravity="center_vertical"
                android:orientation="vertical">

                <TextView
                    android:gravity="center"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:text="@string/license" />

                <com.jaredrummler.materialspinner.MaterialSpinner
                    android:id="@+id/set_upload_licenses"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content" />
            </LinearLayout>


            <!-- Videos languages -->
            <LinearLayout
                android:layout_marginTop="10dp"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center_vertical"
                android:layout_gravity="center_vertical"
                android:orientation="vertical">

                <TextView
                    android:gravity="center"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:text="@string/language" />

                <com.jaredrummler.materialspinner.MaterialSpinner
                    android:id="@+id/set_upload_languages"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content" />
            </LinearLayout>

            <!-- Videos Privacy -->
            <LinearLayout
                android:layout_marginTop="10dp"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center_vertical"
                android:layout_gravity="center_vertical"
                android:orientation="vertical">

                <TextView
                    android:gravity="center"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:text="@string/action_privacy" />

                <com.jaredrummler.materialspinner.MaterialSpinner
                    android:id="@+id/set_upload_privacy"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content" />
            </LinearLayout>


            <!-- More options -->
            <LinearLayout
                android:layout_marginTop="20dp"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center_vertical"
                android:layout_gravity="center_vertical"
                android:orientation="vertical">

                <CheckBox
                    android:id="@+id/set_upload_nsfw"
                    android:layout_width="wrap_content"
                    android:text="@string/peertube_nsfw"
                    android:layout_height="wrap_content" />

                <CheckBox
                    android:id="@+id/set_upload_enable_comments"
                    android:layout_width="wrap_content"
                    android:text="@string/peertube_enable_comments"
                    android:layout_height="wrap_content" />
            </LinearLayout>


            <LinearLayout
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:orientation="horizontal"
                android:layout_marginTop="40dp"
                android:layout_gravity="center">

                <!-- Videos upload edit submit -->
                <Button
                    android:gravity="center"
                    android:layout_gravity="center_horizontal"
                    android:id="@+id/set_upload_delete"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"

                    android:backgroundTint="@color/unfollow"
                    style="@style/Base.Widget.AppCompat.Button.Colored"
                    android:layout_margin="10dp"
                    android:text="@string/delete_video" />

                <!-- Videos upload edit submit -->
                <Button
                    android:gravity="center"
                    android:layout_gravity="center_horizontal"
                    android:id="@+id/set_upload_submit"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_margin="10dp"
                    android:enabled="false"
                    style="@style/Base.Widget.AppCompat.Button.Colored"
                    android:text="@string/update_video" />
            </LinearLayout>

        </LinearLayout>
    </ScrollView>
</androidx.constraintlayout.widget.ConstraintLayout>
